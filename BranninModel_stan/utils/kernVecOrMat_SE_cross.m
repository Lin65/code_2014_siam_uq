function kK = kernVecOrMat_SE( scnLeft, scnRight, sigma_GP, covlen_GP, design_Left, design_Right, sigma_meas )

nLeft = length( scnLeft );
nRight = length( scnRight );

if min(nLeft, nRight) == 0
    kK = 0;
    return;
end;

kK = zeros( nLeft, nRight );

for i = 1 : nLeft
    for j = 1 : nRight
    
        r = (design_Left(scnLeft(i)).x1 - design_Right(scnRight(j)).x1)^2 + (design_Left(scnLeft(i)).x2 - design_Right(scnRight(j)).x2)^2;
        kK(i,j) = sigma_GP^2 * exp( -1/2*(r/covlen_GP^2) );
        if( r == 0 )
            kK(i,j) = kK(i,j) + sigma_meas;
        end;
        
    end;
end;