clear all;
close all;
clc;

%% ------------------------------------------------------------------------
% Get global data by running "main_model.m"
%--------------------------------------------------------------------------


mainsetting;

my_paths='data_figures';

%% ------------------------------------------------------------------------
% Generate also a Random Strategy
%--------------------------------------------------------------------------

%% ------------------------------------------------------------------------
% Load results from strategies
%--------------------------------------------------------------------------
% 
load(fullfile(my_paths, 'results_strategy_1.mat'));

mean_Post_entropy_1 = mean_Post_entropy;
mean_Post_entropy1_1 = mean_Post_entropy1;
mean_KL_1 = mean_KL;
mean_KL1_1 = mean_KL1;
% mean_Post_mean_modelerror_1 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_1 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_1 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_1 = mean_Post_std_correlationlength;
eval_error_1 = eval_error;
eval_std_1 = eval_std;
area_metric_1 = area_metric_new;
index_1 = index_obs;
%--------------------------------------------------------------------------

load(fullfile(my_paths, 'results_strategy_2.mat'));

mean_Post_entropy_2 = mean_Post_entropy;
mean_Post_entropy1_2 = mean_Post_entropy1;
mean_KL_2 = mean_KL;
mean_KL1_2 = mean_KL1;
% mean_Post_mean_modelerror_2 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_2 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_2 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_2 = mean_Post_std_correlationlength;
eval_error_2 = eval_error;
eval_std_2 = eval_std;
area_metric_2 = area_metric_new;
index_2 = index_obs;
% 
% %--------------------------------------------------------------------------

% load([my_paths '\results_strategy_3.mat']);
% 
% mean_Post_entropy_3 = mean_Post_entropy;
% mean_Post_entropy1_3 = mean_Post_entropy1;
% mean_KL_3 = mean_KL;
% mean_KL1_3 = mean_KL1;
% % mean_Post_mean_modelerror_3 = mean_Post_mean_modelerror;
% % mean_Post_std_modelerror_3 = mean_Post_std_modelerror;
% % mean_Post_mean_correlationlength_3 = mean_Post_mean_correlationlength;
% % mean_Post_std_correlationlength_3 = mean_Post_std_correlationlength;
% eval_error_3 = eval_error;
% eval_std_3 = eval_std;
% area_metric_3 = area_metric_new;
% index_3 = index_obs;

% % %--------------------------------------------------------------------------

load(fullfile(my_paths, 'results_strategy_4.mat'));

mean_Post_entropy_4 = mean_Post_entropy;
mean_Post_entropy1_4 = mean_Post_entropy1;
mean_KL_4 = mean_KL;
mean_KL1_4 = mean_KL1;
% mean_Post_mean_modelerror_4 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_4 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_4 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_4 = mean_Post_std_correlationlength;
eval_error_4 = eval_error;
eval_std_4 = eval_std;
area_metric_4 = area_metric_new;
index_4 = index_obs;
% %--------------------------------------------------------------------------
% 
load(fullfile(my_paths, 'results_strategy_5.mat'));

mean_Post_entropy_5 = mean_Post_entropy;
mean_Post_entropy1_5 = mean_Post_entropy1;
mean_KL_5 = mean_KL;
mean_KL1_5 = mean_KL1;
% mean_Post_mean_modelerror_5 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_5 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_5 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_5 = mean_Post_std_correlationlength;
eval_error_5 = eval_error;
eval_std_5 = eval_std;
area_metric_5 = area_metric_new;
index_5 = index_obs;
% 
% %--------------------------------------------------------------------------
% 
load(fullfile(my_paths, 'results_strategy_6.mat'));
% 
mean_Post_entropy_6 = mean_Post_entropy;
mean_Post_entropy1_6 = mean_Post_entropy1;
mean_KL_6 = mean_KL;
mean_KL1_6 = mean_KL1;
% mean_Post_mean_modelerror_6 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_6 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_6 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_6 = mean_Post_std_correlationlength;
eval_error_6 = eval_error;
eval_std_6 = eval_std;
area_metric_6 = area_metric_new;
index_6 = index_obs;

% 
% %--------------------------------------------------------------------------
% 
load(fullfile(my_paths, 'results_strategy_7.mat'));

mean_Post_entropy_7 = mean_Post_entropy;
mean_Post_entropy1_7 = mean_Post_entropy1;
mean_KL_7 = mean_KL;
mean_KL1_7 = mean_KL1;
% mean_Post_mean_modelerror_7 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_7 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_7 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_7 = mean_Post_std_correlationlength;
eval_error_7 = eval_error;
eval_std_7 = eval_std;
area_metric_7 = area_metric_new;
index_7 = index_obs;

% %--------------------------------------------------------------------------

% load([my_paths '\results_strategy_9.mat']);
% 
% mean_Post_entropy_9 = mean_Post_entropy;
% mean_Post_entropy3_9 = mean_Post_entropy3;
% mean_KL_9 = mean_KL;
% mean_KL3_9 = mean_KL3;
% mean_Post_mean_modelerror_9 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_9 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_9 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_9 = mean_Post_std_correlationlength;
% eval_error_9 = eval_error;
% eval_std_9 = eval_std;
% area_metric_9 = area_metric;
% 
% load([my_paths '\results_strategy_10.mat']);
% 
% mean_Post_entropy_10 = mean_Post_entropy;
% mean_Post_entropy3_10 = mean_Post_entropy3;
% mean_KL_10 = mean_KL;
% mean_KL3_10 = mean_KL3;
% mean_Post_mean_modelerror_10 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_10 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_10 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_10 = mean_Post_std_correlationlength;
% eval_error_10 = eval_error;
% eval_std_10 = eval_std;
% area_metric_10 = area_metric;
% 
% load([my_paths '\results_strategy_11.mat']);
% 
% mean_Post_entropy_11 = mean_Post_entropy;
% mean_Post_entropy3_11 = mean_Post_entropy3;
% mean_KL_11 = mean_KL;
% mean_KL3_11 = mean_KL3;
% mean_Post_mean_modelerror_11 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_11 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_11 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_11 = mean_Post_std_correlationlength;
% eval_error_11 = eval_error;
% eval_std_11 = eval_std;
% area_metric_11 = area_metric;
% 
% %--------------------------------------------------------------------------
% 
load(fullfile(my_paths, 'results_strategy_8.mat'));

mean_Post_entropy_8 = mean_Post_entropy;
mean_Post_entropy1_8 = mean_Post_entropy1;
mean_KL_8 = mean_KL;
mean_KL1_8 = mean_KL1;
% mean_Post_mean_modelerror_8 = mean_Post_mean_modelerror;
% mean_Post_std_modelerror_8 = mean_Post_std_modelerror;
% mean_Post_mean_correlationlength_8 = mean_Post_mean_correlationlength;
% mean_Post_std_correlationlength_8 = mean_Post_std_correlationlength;
eval_error_8 = eval_error;
eval_std_8 = eval_std;
area_metric_8 = area_metric_new;
index_8 = index_obs;

%% ------------------------------------------------------------------------
% Plot summary results
%--------------------------------------------------------------------------

figure;
plot(mean_Post_entropy_1);
hold on;
plot(mean_Post_entropy_2,'r');
%plot(mean_Post_entropy_3,'g');
plot(mean_Post_entropy_4,'y');
plot(mean_Post_entropy_5,'c');
plot(mean_Post_entropy_6,'m');
plot(mean_Post_entropy_7,'k');
plot(mean_Post_entropy_8,'--');
%plot(mean_Post_entropy_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('post entropy'); 
set(gca,'XTick',[1:1:num_stages]); 
title('post entropy of 5 parameters');
saveas(gcf,[my_paths,'\main_results\postEntropy.eps'],'psc2');

figure;
plot(mean_Post_entropy1_1);
hold on;
plot(mean_Post_entropy1_2,'r');
%plot(mean_Post_entropy1_3,'g');
plot(mean_Post_entropy1_4,'y');
plot(mean_Post_entropy1_5,'c');
plot(mean_Post_entropy1_6,'m');
plot(mean_Post_entropy1_7,'k');
plot(mean_Post_entropy1_8,'--');
%plot(mean_Post_entropy3_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('post entropy'); 
set(gca,'XTick',[1:1:num_stages]); 
title('post entropy of 3 parameters');
saveas(gcf,[my_paths,'\main_results\postEntropy3.eps'],'psc2');

figure;
plot(mean_KL_1);
hold on;
plot(mean_KL_2,'r');
%plot(mean_KL_3,'g');
plot(mean_KL_4,'y');
plot(mean_KL_5,'c');
plot(mean_KL_6,'m');
plot(mean_KL_7,'k');
plot(mean_KL_8,'--');
%plot(mean_KL_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('KL'); 
set(gca,'XTick',[1:1:num_stages]); 
title('KL');
saveas(gcf,[my_paths,'\main_results\KL.eps'],'psc2');

% figure;
plot(mean_KL1_1);
hold on;
plot(mean_KL1_2,'r');
%plot(mean_KL1_3,'g');
plot(mean_KL1_4,'y');
plot(mean_KL1_5,'c');
plot(mean_KL1_6,'m');
plot(mean_KL1_7,'k');
plot(mean_KL1_8,'--');
%plot(mean_KL1_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('KL'); 
set(gca,'XTick',[1:1:num_stages]); 
title('KL3');
saveas(gcf,[my_paths,'\main_results\KL3.eps'],'psc2');

figure;
plot(eval_error_1);
hold on;
plot(eval_error_2,'r');
%plot(eval_error_3,'g');
plot(eval_error_4,'y');
plot(eval_error_5,'c');
plot(eval_error_6,'m*');
plot(eval_error_7,'k');
plot(eval_error_8,'k--');
%plot(eval_error_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('prediction error'); 
set(gca,'XTick',[1:1:num_stages]); 
title('prediction error');
saveas(gcf,[my_paths,'\main_results\eval_error.eps'],'psc2');


figure;
plot(eval_std_1);
hold on;
plot(eval_std_2,'r');
%plot(eval_std_3,'g');
plot(eval_std_4,'y');
plot(eval_std_5,'c');
plot(eval_std_6,'m*');
plot(eval_std_7,'k');
plot(eval_std_8,'--');
%plot(eval_std_9,'-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('prediction std'); 
set(gca,'XTick',[1:1:num_stages]); 
title('prediction std');
saveas(gcf,[my_paths,'\main_results\eval_std.eps'],'psc2');

figure;
plot(area_metric_1);
hold on;
plot(area_metric_2,'r');
%plot(area_metric_3,'g');
plot(area_metric_4,'y');
plot(area_metric_5,'c');
plot(area_metric_6,'m*');
plot(area_metric_7,'k');
plot(area_metric_8,'--');
%plot(area_metric_9,'-.');
%plot(area_metric_10,'k-.');
%legend('maximin','MI','hybrid: adaptive KL');
legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('average area metric'); 
set(gca,'XTick',[1:1:num_stages]); 
title('average area metric');
saveas(gcf,[my_paths,'\main_results\average_area_metric.eps'],'psc2');


