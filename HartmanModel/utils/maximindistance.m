function no = maximindistance(listTarget, listLeft,design)

n = size(listLeft,2);
m = size(listTarget,2);
dist = zeros(1,n);

for i = 1:n
    mindistance = inf;
    for j = 1:m
        distance = abs((design(listLeft(i)).x1 - design(listTarget(j)).x1)^2 + (design(listLeft(i)).x2 - design(listTarget(j)).x2)^2 );
        if distance < mindistance	
		   mindistance = distance;
		end;
    end;
    dist(i) = mindistance;
end;	

noAll = find (dist == max(dist));
no = noAll(1);

