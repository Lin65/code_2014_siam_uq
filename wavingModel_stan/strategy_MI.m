%%---------------------------------------------
%  full maximal mutual information strategy
%%---------------------------------------------


clear all;
close all;
clc;

addpath('utils');
paths=[pwd,'\data_figures\strategy_2\'];

%% ------------------------------------------------------------------------
% Strategy number
%--------------------------------------------------------------------------

no_strategy = 2;

%% ------------------------------------------------------------------------
% Load measurements
% Get global data by running "mainsetting.m"
%--------------------------------------------------------------------------

  load measurement.mat;
  mainsetting;


%% ------------------------------------------------------------------------
% strategy setting
%--------------------------------------------------------------------------


%% ------------------------------------------------------------------------
% Main Simulation
%--------------------------------------------------------------------------

trials_Post_entropy = zeros(1, num_stages);
trials_Post_entropy1 = zeros(1, num_stages);
trials_Post_mean_modelerror = zeros(1, num_stages);
trials_Post_std_modelerror = zeros(1, num_stages);
trials_Post_mean_correlationlength_x = zeros(1, num_stages);
trials_Post_std_correlationlength_x = zeros(1, num_stages);
trials_Post_mean_correlationlength_y = zeros(1, num_stages);
trials_Post_std_correlationlength_y = zeros(1, num_stages);
trials_KL = zeros(1, num_stages);
trials_KL1 = zeros(1, num_stages);

trials_MI_est = zeros(1, num_stages);

trials_p1_mean = zeros(1, num_stages);
trials_p1_std = zeros(1, num_stages);

MI_est = zeros(1,num_designs);
my_state = zeros(1,init_filter.no_particles);
my_chain_data = zeros( init_filter.no_particles,1 );  
prediction_state = zeros(init_filter.no_particles, size(prediction_design,2));
prediction_data = zeros( init_filter.no_particles, size(prediction_design,2)); 

eval_error = zeros(1, num_stages);
eval_std = zeros(1, num_stages);
area_metric = zeros(1, num_stages);
area_metric_new = zeros(1, num_stages);

update_samples = cell(1, num_stages);
instant_prediction = cell(1, num_stages);
indexMI = zeros(1,num_stages);

%fprintf( '   |- Process trial %d / %d\n', 1, num_trials);
% initial distribution used by KL   
%rand('seed',101);
%randn('seed',101); 
% p_pf_prop = repmat( p_bounds(:,1), 1, init_filter.no_particles ) + repmat( p_bounds(:,2)- p_bounds(:,1), 1, init_filter.no_particles ) .* rand( 4, init_filter.no_particles );
% p_pf_initial = p_pf_prop;	

% initializations
MI_est(:) = 0;
options = 1:num_designs;
	
for k_stage = 1 : num_stages	    
	maxMI = -inf; 
	indMax = 0; 
    Am = zeros(1,size(prediction_x,2));
    Am_new = zeros(1,size(prediction_x,2));

    for k_design = options
		fprintf( '   |-- Process design %d / %d\n', k_design, num_designs );		  	
	%	    rand('seed',101);
    %randn('seed',101);    
		if (k_stage == 1)
            rand('seed',1001);
            randn('seed',1001);
		    for i = 1 : init_filter.no_particles		
		        %my_state(i) = p_pf_prop(1,i) + p_pf_prop(2,i)*design(k_design).x1 + p_pf_prop(3,i)*design(k_design).x2 + simulateMeas( 0, p_pf_prop(3,i) );
			    my_state(i) = Simpuff(design(k_design).x, design(k_design).y, p_pf_prop(1:3,i )) + simulateMeas( 0, p_pf_prop(4,i) );
                my_chain_data(i,:) = simulateMeas( my_state(i), meas_std );
		    end;
%              % prediction data in prediction region used to calculate MI(obs of meas domain; p of prediction domain)	
% 		    for i = 1 : init_filter.no_particles		
% 		        %my_state(i) = p_pf_prop(1,i) + p_pf_prop(2,i)*design(k_design).x1 + p_pf_prop(3,i)*design(k_design).x2 + simulateMeas( 0, p_pf_prop(3,i) );
% 			    for j = 1 : size(prediction_design,2)
%                     prediction_state(i,j) = Simpuff(prediction_design(j).x, prediction_design(j).y, p_pf_prop(1:3,i )) + simulateMeas( 0, p_pf_prop(4,i) );
%                     prediction_data(i,j) = simulateMeas( prediction_state(i,j), meas_std );
%                 end;
% 		    end;
            my_chain = [my_chain_data, p_pf_prop(:,:)']; 
		end;			
		% GP prediction
        rng('default');
		if (k_stage > 1)			
			training_x = x_sofar;
			training_obs = obs_sofar;
			test_x = k_design;	
            test_design = design;	
    %rand('seed',101);
    %randn('seed',101);
			my_chain_data = GPprediction(p_pf_prop, training_x, training_obs, test_x, design, test_design, R,1);
%             % prediction data in prediction region used to calculate MI(obs of meas domain; p of prediction domain)	
%             prediction_data = GPprediction(p_pf_prop, training_x, training_obs, prediction_x, design, prediction_design, R,1);
            my_chain = [my_chain_data, p_pf_prop(:,:)'];  
	    end;
        
		%my_chain = [my_chain_data, prediction_data];                          % particles of both states and observations			
	    MI_est( k_design ) = knnMI_GaussApprox( my_chain, [1,6]);
		   				
		if ( MI_est( k_design ) > maxMI )
	        maxMI = MI_est( k_design );
		    indMax = k_design;
			my_state_max = my_state;
	    end;		
	end;
			
	% Bayes update - get also the posterior
	fprintf( '      |- Bayesian update\n' );		
	indexMI(1,k_stage) = indMax
		
    % MCMC update 		
	index_obs = indexMI(1,1:k_stage);
	x_sofar = index_obs;
	obs_sofar = meas(1,index_obs);
	%    rand('seed',101);
    %randn('seed',101);	
    P_pf_updateRaw = stan_MCMCupdateGP (p_bounds, x_sofar, obs_sofar, design, R, init_filter.no_particles);
    P_pf_update = P_pf_updateRaw(:,:);
	update_samples{k_stage} = P_pf_update;	
	%plot prediction
    rand('seed',1001);
    randn('seed',1001);
	instant_prediction{k_stage} = GPprediction(P_pf_update, x_sofar, obs_sofar, prediction_x, design, prediction_design, R,0);
    for i =1:size(prediction_x,2)
        [CDF,Points] = ecdf(instant_prediction{k_stage}(:,i));
        % area metric
        points_uniform = linspace(min(Points),max(Points),init_filter.no_particles);
        for j = 1:init_filter.no_particles
            cdf_new(j) = CDF(max(find(Points<=points_uniform(j))));
        end;
            index1_new = find(points_uniform<truth_prediction(i));
            index2_new = find(points_uniform>truth_prediction(i));
            if (numel(index1_new)<2)
                area1_new = 0;
            else 
                area1_new = trapz(points_uniform(index1_new),cdf_new(index1_new));
            end;
            
            if (numel(index2_new)<2)
                area2_new = 0;
            else
                area2_new = trapz(points_uniform(index2_new),1 - cdf_new(index2_new));
            end;
            
            Am_new(i) = area1_new + area2_new;
			
            index1 = find(Points<truth_prediction(i));
            index2 = find(Points>truth_prediction(i));
            if (numel(index1)<2)
                area1 = 0;
            else 
                area1 = trapz(Points(index1),CDF(index1));
            end;
            
            if (numel(index2)<2)
                area2 = 0;
            else
                area2 = trapz(Points(index2),1 - CDF(index2));
            end;
            
            Am(i) = area1 + area2;            %Am(i) = trapz(Points(Points<truth_prediction(i)),CDF(Points<truth_prediction(i))) + trapz(Points(Points>truth_prediction(i)),1 - CDF(Points>truth_prediction(i))); 

%            lowerbound(i) = Points(max(find(CDF<0.025))+1);
%            upperbound(i) = Points(min(find(CDF>0.975))-1);
    end;
       
	area_metric(k_stage) = sum(Am)/size(prediction_x,2);
    area_metric_new(k_stage) = (sum(Am_new))/size(prediction_x,2);       
%         figure;% validation metric
%         cdfplot(instant_prediction(:,i));
%         set(findobj(get(gca,'Children'),'LineWidth',0.5),'LineWidth',2);
%         plot([truth(i) truth(i)],[0 1],'r','LineWidth',2);
%         legend('prediction distribution','the truth');
        
    pred_mean = mean(instant_prediction{k_stage});
    pred_std =  std(instant_prediction{k_stage});
    eval_error(k_stage) = sqrt(sum(abs(pred_mean - truth_prediction).^2)/numel(pred_mean));
    eval_std(k_stage) =  sum(pred_std)/numel(pred_mean);    
    
%    prediction_lowerbound = reshape( lowerbound, size(prediction_X_grid,1), size(prediction_Y_grid,2) );
%    prediction_upperbound = reshape( upperbound, size(prediction_X_grid,1), size(prediction_Y_grid,2) );
%  		
%    figure;       
%    surf(prediction_X_grid,prediction_Y_grid,prediction_lowerbound);
%    hold on;
%    surf(prediction_X_grid,prediction_Y_grid,prediction_upperbound);
%    mesh(prediction_X_grid, prediction_Y_grid, truth_prediction_grid);
% 	title({'MI: instant prediction at stage ',num2str(k_stage)});
%    xlabel('x');
%    ylabel('y');
%    zlabel('z');
%    saveas(gcf,[paths,'fig',num2str(k_stage),'.jpg']);		

	p_pf_update = P_pf_update;
	
    trials_MI_est(1,k_stage) = MI_est(indMax);                	
	trials_Post_entropy(1, k_stage) = knnENT_GaussApprox( p_pf_update(:,:)');
	trials_Post_entropy1(1, k_stage) = knnENT_GaussApprox( p_pf_update(1:3,:)' );			
	trials_Post_mean_modelerror(1, k_stage) = mean(p_pf_update(4,:)');
    trials_Post_std_modelerror(1, k_stage) = std(p_pf_update(4,:)');
    trials_Post_mean_correlationlength_x(1, k_stage) = mean(p_pf_update(5,:)');
    trials_Post_std_correlationlength_x(1, k_stage)= std(p_pf_update(5,:)');
    trials_Post_mean_correlationlength_y(1, k_stage) = mean(p_pf_update(6,:)');
    trials_Post_std_correlationlength_y(1, k_stage)= std(p_pf_update(6,:)');
    trials_KL(1, k_stage) = knnKL_GaussApprox(p_pf_prop, p_pf_update, 6);
    trials_KL1(1, k_stage) = knnKL_GaussApprox(p_pf_prop(1:3,:), p_pf_update(1:3,:), 3);
	
% 	trials_p1_mean(1, k_stage) = mean(p_pf_update(1,:)');
%    % trials_p2_mean(1, k_stage) = mean(p_pf_update(2,:)');
%     trials_p1_std(1, k_stage) = std(p_pf_update(1,:)');
%    % trials_p2_std(1, k_stage) = std(p_pf_update(2,:)');
	
	p_pf_prop = p_pf_update;
	options(find(options == indMax)) = [];
		
end;



mean_MI_est = trials_MI_est; 
mean_Post_entropy = trials_Post_entropy;
mean_Post_entropy1 = trials_Post_entropy1;   
mean_KL = trials_KL;
mean_KL1 = trials_KL1;
mean_Post_mean_modelerror = trials_Post_mean_modelerror;
mean_Post_std_modelerror = trials_Post_std_modelerror;
mean_Post_mean_correlationlength_x = trials_Post_mean_correlationlength_x;
mean_Post_std_correlationlength_x = trials_Post_std_correlationlength_x;
mean_Post_mean_correlationlength_y = trials_Post_mean_correlationlength_y;
mean_Post_std_correlationlength_y = trials_Post_std_correlationlength_y;

% figure;
% plot(trials_p1_mean);
% hold on;
% upper = trials_p1_mean + 2*trials_p1_std;
% lower = trials_p1_mean - 2*trials_p1_std;
% plot(upper);
% plot(lower);
% plot([1 20], [x0 x0],'r' );
% title('MI:estimation of p1');
% saveas(gcf,[paths,'estimation of p1.jpg']);
% saveas(gcf,[paths,'estimation of p1.eps']);
% 
% 
% figure;
% plot(mean_Post_entropy);
% title('MI:post entropy of all parameters');
% saveas(gcf,[paths,'post entropy of all.jpg']);
% saveas(gcf,[paths,'post entropy of all.eps']);
% 
% 
%     figure;
%     plot(mean_Post_entropy1);
%     title('MI: post entropy 1');
% 	saveas(gcf,[paths,'post entropy 1.jpg']);
%     saveas(gcf,[paths,'post entropy 1.eps']);
% 	
%     figure;
%     plot(mean_KL);
%     title('MI: KL');
%     saveas(gcf,[paths,'KL.jpg']);
%     saveas(gcf,[paths,'KL.eps']);
% 	
%     figure;
%     plot(mean_KL1);
%     title('MI: KL1');
%     saveas(gcf,[paths,'KL1.jpg']);
%     saveas(gcf,[paths,'KL1.eps']);
% 	
figure;
plot(eval_error);
title('MI: prediction error');
saveas(gcf,[paths,'prediction error.jpg']);
saveas(gcf,[paths,'prediction error.eps']);

figure;
plot(eval_std);
title('MI: prediction std');
saveas(gcf,[paths,'prediction std.jpg']);
saveas(gcf,[paths,'prediction std.eps']);

figure;
plot(area_metric_new);
title('MI: area metric');
saveas(gcf,[paths,'area metric.jpg']);
saveas(gcf,[paths,'area metric.eps']);

save(['data_figures/results_strategy_' num2str(no_strategy) '.mat']);

%strategy_segmentation