model = StanModel('verbose',true);
model.compile('stanc');

code = {
    'data {'
        'int  N;'
        'real x;'
        'real y;'
        'real d;'
    '}'
    'parameters {'
        'real theta1;'                        
        'real theta2;'             
        'real theta3;'                   
        'real<lower=0.0000001> theta4;'  
        'real<lower=0.0000001> theta5;'
        'real<lower=0.0000001> theta6;'
    '}'
    'transformed parameters {'
        'real mu;'
        'real Sigma;'
        'mu <- theta1 + theta2*x + theta3*y;'  
        'Sigma <- fmax(0.0000001,pow(theta4,2));' 
    '}'
    'model {'
        'theta1 ~ normal(0.0, 10);'
        'theta2 ~ normal(0.0, 10);'
        'theta3 ~ normal(0.0, 10);'
        'theta4 ~ normal(0.0, 0.5);'
        'theta5 ~ normal(0.0, 0.5);'
        'theta6 ~ normal(0.0, 0.5);'
        'd ~ normal(mu, Sigma);'
    '}'
};


model.set('model_code',code,'model_name','first_halfwaving');
model.compile();
