function prediction_data = GPprediction(p_pf_prop, training_x_index, training_y, test_x_index, design, test_design, R , obs_sign)

training_x = zeros(2, numel(training_x_index));
test_x = zeros(2,numel(test_x_index));

for i = 1:numel(training_x_index)
    training_x(1,i) = design(training_x_index(i)).x1;
    training_x(2,i) = design(training_x_index(i)).x2;
end;

for i = 1:numel(test_x_index)
    test_x(1,i) = test_design(test_x_index(i)).x1;
    test_x(2,i) = test_design(test_x_index(i)).x2;
end;

no_smps = size(p_pf_prop, 2);

prediction_data = zeros(no_smps,numel(test_x_index));

for i = 1:no_smps
    mean_test = (repmat(p_pf_prop(1,i),size(test_x,2),1) + repmat(p_pf_prop(2,i),size(test_x,2),1).*test_x(1,:)' + repmat(p_pf_prop(3,i),size(test_x,2),1).*test_x(2,:)')' ;
	mean_train = (repmat(p_pf_prop(1,i),size(training_x,2),1) + repmat(p_pf_prop(2,i),size(training_x,2),1).*training_x(1,:)' + repmat(p_pf_prop(3,i),size(training_x,2),1).*training_x(2,:)')';
    
    KK_train = kernVecOrMat_SE( training_x_index', training_x_index', p_pf_prop(4,i), p_pf_prop(5,i),design, design,R);
	if obs_sign ==0
       KK_test = kernVecOrMat_SE( test_x_index', test_x_index', p_pf_prop(4,i), p_pf_prop(5,i),test_design, test_design, 0);
	else
       KK_test = kernVecOrMat_SE( test_x_index', test_x_index', p_pf_prop(4,i), p_pf_prop(5,i),test_design, test_design, R);
	end;
	   
    KK_test_train = kernVecOrMat_SE( test_x_index', training_x_index', p_pf_prop(4,i), p_pf_prop(5,i),test_design,design, 0);
   
    mean_predict = mean_test + (KK_test_train*inv(KK_train)*(training_y' - mean_train'))';
    cov_test = KK_test - KK_test_train*inv(KK_train)*KK_test_train';
   % Std_predict(i,:) = sqrt(diag(cov_test));
%     diag_cov = diag(cov_test);
%     diag_cov(diag_cov < 0.0001) = 0.0001;
    cov_test =  cov_test + diag(abs(diag( cov_test))*0.0001 + 0.000001);
    %test = diag(cov_test)

    prediction_data(i,:) = mean_predict +(chol(cov_test)'*randn(size(test_x,2),1))';
end;
