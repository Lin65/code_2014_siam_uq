%%---------------------------------------------
%  hybrid: adaptive : KL decreases dramatically then explore the space
%%---------------------------------------------


clear all;
close all;
clc;

addpath('utils');
paths=[pwd,'\data_figures\strategy_8\'];

%% ------------------------------------------------------------------------
% Strategy number
%--------------------------------------------------------------------------

no_strategy = 8;

%% ------------------------------------------------------------------------
% Load measurements
% Get global data by running "mainsetting.m"
%--------------------------------------------------------------------------

load measurement.mat;
mainsetting;


%% ------------------------------------------------------------------------
% strategy setting
%--------------------------------------------------------------------------

hybrid_a = zeros(1,num_stages);
threshold = 0.5;


%% ------------------------------------------------------------------------
% Main Simulation
%--------------------------------------------------------------------------

trials_Post_entropy = zeros(num_trials, num_stages);
trials_Post_entropy1 = zeros(num_trials, num_stages);
trials_Post_mean_modelerror = zeros(num_trials, num_stages);
trials_Post_std_modelerror = zeros(num_trials, num_stages);
trials_Post_mean_correlationlength_x = zeros(num_trials, num_stages);
trials_Post_std_correlationlength_x = zeros(num_trials, num_stages);
trials_Post_mean_correlationlength_y = zeros(num_trials, num_stages);
trials_Post_std_correlationlength_y = zeros(num_trials, num_stages);
trials_KL = zeros(num_trials, num_stages);
trials_KL1 = zeros(num_trials, num_stages);
trials_MI_est = zeros(num_trials, num_stages);
trials_eval_error = zeros(num_trials, num_stages);
trials_eval_std = zeros(num_trials, num_stages);
trials_area_metric = zeros(num_trials, num_stages);
trials_area_metric_new = zeros(num_trials, num_stages);

MI_est = zeros(1, num_designs);
min_distance = zeros(1, num_designs);
my_state = zeros(1,init_filter.no_particles);
my_chain_data = zeros( init_filter.no_particles,1 );  
prediction_state = zeros(init_filter.no_particles, size(prediction_design,2));
prediction_data = zeros( init_filter.no_particles, size(prediction_design,2)); 
indexMI = zeros(1,num_stages);

update_samples = cell(num_trials, num_stages);
instant_prediction = cell(num_trials, num_stages);

for trial = 1: num_trials
    fprintf( '   |- Process trial %d / %d\n', trial, num_trials); 
    % initializations
    MI_est(:) = 0;
    options = 1:num_designs;

    for k_stage = 1 : num_stages	    
        fprintf( '   |------ Process stage %d / %d\n', k_stage, num_stages);        
        MI_est(:) = 0;
        min_distance(:) = 0;
        Am = zeros(1,size(prediction_x,2));
        Am_new = zeros(1,size(prediction_x,2));
		
        for k_design = options
            fprintf( '   |----------- Process design %d / %d\n', k_design, num_designs );		  	
		    % propagate all the particles to corresponding time period

            if (k_stage == 1)
                for i = 1 : init_filter.no_particles		
                    my_state(i) = Simpuff(design(k_design).x, design(k_design).y, p_pf_prop(1:3,i )) + simulateMeas( 0, p_pf_prop(4,i) );
                    my_chain_data(i,:) = simulateMeas( my_state(i), meas_std );
                end;
%                 % prediction data in prediction region used to calculate MI(obs of meas domain; p of prediction domain)	
%                 for i = 1 : init_filter.no_particles		
%                     for j = 1 : size(prediction_design,2)
%                         prediction_state(i,j) = Simpuff(prediction_design(j).x, prediction_design(j).y, p_pf_prop(1,i )) + simulateMeas( 0, p_pf_prop(2,i) );
%                         prediction_data(i,j) = simulateMeas( prediction_state(i,j), meas_std );
%                     end;
%                 end;
%                my_chain = [my_chain_data, prediction_data]; 
            end;			

            if (k_stage > 1)			
                training_x = x_sofar;
                training_obs = obs_sofar;
                test_x = k_design;
                test_design = design;
                
                rand('seed',1001);
                randn('seed',1001);
                my_chain_data = GPprediction(p_pf_prop, training_x, training_obs, test_x, design, test_design, R,1);				
%                 % prediction data in prediction region used to calculate MI(obs of meas domain; p of prediction domain)	
%                 prediction_data = GPprediction(p_pf_prop, training_x, training_obs, prediction_x, design, prediction_design, R,1);
%                 my_chain = [my_chain_data, prediction_data]; 
                options_used = indexMI(1,1:(k_stage-1));			
                min_distance( k_design) = mindistance( options_used, k_design, design_scaled);
            end;
		
            my_chain = [my_chain_data,p_pf_prop(:,:)'];                          % particles of both states and observations			
            MI_est( k_design ) = knnMI_GaussApprox( my_chain,[1,6]);         			
        end;
       
        %-----------------------------------------------------------
        % For different strategies, only the value of hybrid_a is different!
        %-----------------------------------------------------------
        if (k_stage > 2)
            if (trials_KL(trial,k_stage-1) < trials_KL(trial,k_stage-2) * threshold)
                hybrid_a(k_stage) = 1;
            else 
                hybrid_a(k_stage) = 0;
            end;
        end;
        
        if (k_stage == 2)
            hybrid_a(k_stage) = 0;
        end;

        %------------------------------------------------------------
        % Finish setting hybrid_a.
        %-------------------------------------------------------------
   
        hybrid_distance = (1 - hybrid_a(k_stage))*MI_est/max(MI_est) + hybrid_a(k_stage) * min_distance/max(min_distance);		
        [max_distance, indMax] = max(hybrid_distance);	
    
        %---------------------------------------------------------------
        % Fix the first two obs points
        %---------------------------------------------------------------
        if (k_stage == 1)
            indMax = 121;
        end;   
        %--------------------------------------------------------------
        %----------------------------------------------------------------			
	    % Bayes update - get also the posterior
        fprintf( '      |----------------- Bayesian update\n' );		
        indexMI(1,k_stage) = indMax;		
        % MCMC update 		
        index_obs = indexMI(1,1:k_stage);
        fprintf( '   |--------------------------- Process strategy %d \n', no_strategy); 
        x_sofar = index_obs
        obs_sofar = meas(1,index_obs);
        
        rng('default');
        p_pf_updateRaw = stan_MCMCupdateGP (p_bounds, x_sofar, obs_sofar, design, R, init_filter.no_particles);
        p_pf_update = p_pf_updateRaw(:,:);
        update_samples{trial,k_stage} = p_pf_update;
		% GP prediction
        
        rand('seed',1001);
        randn('seed',1001);
        instant_prediction{trial,k_stage} = GPprediction(p_pf_update, x_sofar, obs_sofar, prediction_x, design, prediction_design, R,0);
        for i =1:size(prediction_x,2)
            [CDF,Points] = ecdf(instant_prediction{trial,k_stage}(:,i));
            points_uniform = linspace(min(Points),max(Points),init_filter.no_particles);
            for j = 1:init_filter.no_particles
                cdf_new(j) = CDF(max(find(Points<=points_uniform(j))));
            end;
            index1_new = find(points_uniform<truth_prediction(i));
            index2_new = find(points_uniform>truth_prediction(i));
            if (numel(index1_new)<2)
                area1_new = 0;
            else 
                area1_new = trapz(points_uniform(index1_new),cdf_new(index1_new));
            end;
            
            if (numel(index2_new)<2)
                area2_new = 0;
            else
                area2_new = trapz(points_uniform(index2_new),1 - cdf_new(index2_new));
            end;
            
            Am_new(i) = area1_new + area2_new;
			
            index1 = find(Points<truth_prediction(i));
            index2 = find(Points>truth_prediction(i));
            if (numel(index1)<2)
                area1 = 0;
            else 
                area1 = trapz(Points(index1),CDF(index1));
            end;
            
            if (numel(index2)<2)
                area2 = 0;
            else
                area2 = trapz(Points(index2),1 - CDF(index2));
            end;
            
            Am(i) = area1 + area2;
            % lowerbound(i) = Points(max(find(CDF<0.025))+1);
            % upperbound(i) = Points(min(find(CDF>0.975))-1);
        end;

        trials_area_metric(trial,k_stage) = sum(Am)/size(prediction_x,2);
        trials_area_metric_new(trial,k_stage) = (sum(Am_new))/size(prediction_x,2);       
       
        pred_mean = mean(instant_prediction{trial,k_stage});
        pred_std =  std(instant_prediction{trial,k_stage});
        trials_eval_error(trial,k_stage) = sqrt(sum(abs(pred_mean - truth_prediction).^2)/numel(pred_mean));
        trials_eval_std(trial,k_stage) =  sum(pred_std)/numel(pred_mean);         
            %prediction_lowerbound = reshape( lowerbound, size(prediction_X1_grid,1), size(prediction_X1_grid,2) );
            %prediction_upperbound = reshape( upperbound, size(prediction_X1_grid,1), size(prediction_X1_grid,2) );
 		
%           figure;       
%           surf(prediction_X1_grid,prediction_X2_grid,prediction_lowerbound);
%           hold on;
%           surf(prediction_X1_grid,prediction_X2_grid,prediction_upperbound);
%           X = [prediction_tspan, fliplr(prediction_tspan)];
%           Y = [upperbound, fliplr(lowerbound)];
%           fill(X,Y,'c');
%           hold on;
%           plot(tspan,truth,'r');
%           title({'hybrid a = 1: instant prediction at stage ',num2str(k_stage)});
%           set(gca,'XTick',tspan); 
%           xlabel('x1');
%           ylabel('x2');
%           zlabel('y');
%           saveas(gcf,[paths,'fig',num2str(k_stage),'.jpg']);		

        trials_MI_est(trial,k_stage) = MI_est(indMax);                	
        trials_Post_entropy(trial, k_stage) = knnENT_GaussApprox( p_pf_update(:,:)');
        trials_Post_entropy1(trial, k_stage) = knnENT_GaussApprox( p_pf_update(1:3,:)' );			
        trials_Post_mean_modelerror(trial, k_stage) = mean(p_pf_update(4,:)');
        trials_Post_std_modelerror(trial, k_stage) = std(p_pf_update(4,:)');
        trials_Post_mean_correlationlength_x(trial, k_stage) = mean(p_pf_update(5,:)');
        trials_Post_std_correlationlength_x(trial, k_stage)= std(p_pf_update(5,:)');
        trials_Post_mean_correlationlength_y(trial, k_stage) = mean(p_pf_update(6,:)');
        trials_Post_std_correlationlength_y(trial, k_stage)= std(p_pf_update(6,:)');
        trials_KL(trial, k_stage) = knnKL_GaussApprox(p_pf_prop, p_pf_update, 6);
        trials_KL1(trial, k_stage) = knnKL_GaussApprox(p_pf_prop(1:3,:), p_pf_update(1:3,:), 3);
		  
        p_pf_prop = p_pf_update;
        options(find(options == indMax)) = [];
    end;
end;
	
if num_trials > 1
    
    mean_MI_est = mean(trials_MI_est); 
    mean_Post_entropy = mean(trials_Post_entropy);
    mean_Post_entropy1 = mean(trials_Post_entropy1);   
    mean_KL = mean(trials_KL);
    mean_KL1 = mean(trials_KL1);
    mean_Post_mean_modelerror = mean(trials_Post_mean_modelerror);
    mean_Post_std_modelerror = mean(trials_Post_std_modelerror);
    mean_Post_mean_correlationlength_x = mean(trials_Post_mean_correlationlength_x);
    mean_Post_std_correlationlength_x = mean(trials_Post_std_correlationlength_x);
    mean_Post_mean_correlationlength_y = mean(trials_Post_mean_correlationlength_y);
    mean_Post_std_correlationlength_y = mean(trials_Post_std_correlationlength_y);
    mean_area_metric = mean(trials_area_metric);
    mean_area_metric_new = mean(trials_area_metric_new);
    mean_eval_error = mean(trials_eval_error);
    mean_eval_std = mean(trials_eval_std);
else
    mean_MI_est = trials_MI_est; 
    mean_Post_entropy = trials_Post_entropy;
    mean_Post_entropy1 = trials_Post_entropy1;   
    mean_KL = trials_KL;
    mean_KL1 = trials_KL1;
    mean_Post_mean_modelerror = trials_Post_mean_modelerror;
    mean_Post_std_modelerror = trials_Post_std_modelerror;
    mean_Post_mean_correlationlength_x = trials_Post_mean_correlationlength_x;
    mean_Post_std_correlationlength_x = trials_Post_std_correlationlength_x;
    mean_Post_mean_correlationlength_y = trials_Post_mean_correlationlength_y;
    mean_Post_std_correlationlength_y = trials_Post_std_correlationlength_y;
    mean_area_metric = trials_area_metric;
    mean_area_metric_new = trials_area_metric_new;
    mean_eval_error = trials_eval_error;
    mean_eval_std = trials_eval_std;   
end;
save(['data_figures/results_strategy_' num2str(no_strategy) '.mat']);

%strategy_adaptive_KL
