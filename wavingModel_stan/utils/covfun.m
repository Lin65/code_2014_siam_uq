function cov = covfun(theta, x1,x2, R)

dim = size(x1,1);
cov = zeros(dim);

for i=1:dim
    for j = 1:dim
        cov(i,j) = theta(2)^2*exp(-0.5*((x1(i) - x1(j))^2)/theta(3)^2)*exp(-0.5*((x2(i) - x2(j))^2)/theta(4)^2);
        if i==j
            cov(i,j) = cov(i,j) + R;
            if(cov(i,j)<1000000*eps) 
                cov(i,j) = 1000000*eps;
            end;
        end;
    end;
end;
 cov
det(cov)