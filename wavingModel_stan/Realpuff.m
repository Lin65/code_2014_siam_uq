function z = Realpuff( x, y )

z = 2 + 0.01*(y - x.^2).^2 + (1 - x).^2 + 2*(2 - y).^2 + 7*sin(0.5*x).*sin(0.7*x.*y);

end




