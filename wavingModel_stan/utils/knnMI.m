function mi = knnMI(my_chain,my_knn,my_split)

[lenChain, dimChain] = size(my_chain);
my_dx = my_split(1);

if( sum(my_split) ~= dimChain )
    error('the joint dimension in the split is different from the dimension of the chain')
end;

%--------------------------------------------------------------------------
% Normalize the chain
%--------------------------------------------------------------------------
mean_chain = mean(my_chain);
std_chain = std(my_chain);

my_chain = (my_chain - repmat(mean_chain,lenChain,1)) ./ repmat(std_chain,lenChain,1);

%--------------------------------------------------------------------------
% Estimate mutual information
%--------------------------------------------------------------------------
% my_knn+1 because we do not want to count the actual point
[nn,dd] = knn(my_chain, my_chain, my_knn+1, inf);    

noPts_x = knnDist(my_chain(:,1:my_dx), my_chain(:,1:my_dx), dd(:,end), inf);
noPts_y = knnDist(my_chain(:,my_dx+1:end), my_chain(:,my_dx+1:end), dd(:,end), inf);

% total number of point contained into the distance = actual point + knn + the rest 
mi = psi(my_knn) + psi(lenChain) - mean(psi(noPts_x+1) + psi(noPts_y+1));
