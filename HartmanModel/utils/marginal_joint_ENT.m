function vec_ent = marginal_joint_ENT(theta_smp, my_knn)

[Nsmp,my_dim] = size(theta_smp);
vec_ent = zeros(1,my_dim+1);

for i = 1 : my_dim
    vec_ent(i) = knnENT(theta_smp(:,i), my_knn);
end;

vec_ent(end) = knnENT(theta_smp, my_knn);