addpath('utils');

%%-------------------------------------------------------------------------
% Initial uncertainty
%--------------------------------------------------------------------------

% uncertainty initial condition --normal distribution
p_bounds = [ 0.0  10              % theta0
	         0.0  10              % theta1 
             0.0  10              % theta2
	         0.0  0.5             % sigma
	         0.0  0.5             % lx
             0.0  0.5];			  % ly

% uncertainty measurements: 3*std = err_perc (error normally distributed)
err_perc =0.00;       % +/- 5% sampling error
meas_std = err_perc;
R = meas_std^2; 


%%-------------------------------------------------------------------------
% true parameters
%--------------------------------------------------------------------------
% K = 1/12*u*x has already been included in the Realpuff model

%% ------------------------------------------------------------------------
% Domain settings
%--------------------------------------------------------------------------
num_stages = 5;


% truth domain is used to generate truth figure with high resolution

x_truth_span = linspace(-5, 10, 50);   
y_truth_span = linspace(0, 15, 50);    
[X_truth_grid, Y_truth_grid] = meshgrid(x_truth_span, y_truth_span);

% meas domain is used to generate meas data and is the domain to do inverse
% problem

x_span = linspace(-5, 10, 11);   
y_span = linspace(0, 15, 11);    
[X_grid, Y_grid] = meshgrid(x_span, y_span);

num_designs = numel(X_grid);

for i = 1:num_designs
    design(i).x = X_grid(i);
    design(i).y = Y_grid(i);
end;

% scale the inputs so that each is sampled from a unit interval
for i = 1:num_designs
    design_scaled(i).x = X_grid(i);
    design_scaled(i).y = Y_grid(i);
end;



%% ------------------------------------------------------------------------
% Filter settings
%--------------------------------------------------------------------------

init_filter.no_particles = 2000;           % no particles         

%% ------------------------------------------------------------------------
% Number of trials
%--------------------------------------------------------------------------

num_trials = 1;

%% ------------------------------------------------------------------------
% For instant prediction
%--------------------------------------------------------------------------
num_prediction = 121;
prediction_x = 1:num_prediction;
prediction_x_span = linspace(-5, 10, 11);   % rw
prediction_y_span = linspace(0, 15, 11);    % Kw
[prediction_X_grid, prediction_Y_grid] = meshgrid(prediction_x_span, prediction_y_span);
for i = 1:num_prediction
    prediction_design(i).x = prediction_X_grid(i);
    prediction_design(i).y = prediction_Y_grid(i);
end;
lowerbound = zeros(1,size(prediction_x,2));
upperbound = zeros(1,size(prediction_x,2));


%----------------------domain----------------------
% (y)
% 10 |10 20 30 40 50 60 70 80 90 100             
% 9  |9  19                      .
% 8  |8  18                      .
% 7  |7  17                      .
% 6  |6  16
% 5  |5  15 25 35 45 55 65 75 85 95
% 4  |4  14                      94
% 3  |3  13                      93
% 2  |2  12                      92
% 1  |1  11 21 31 41 51 61 71 81 91                
%     _  _  _  _  _  _  _  _  _  _
%     10 20 30 40 50 60 70 80 90 100 (x)
%--------------------------------------------------







