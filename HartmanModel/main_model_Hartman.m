

addpath('utils');

%%-------------------------------------------------------------------------
% Initial uncertainty
%--------------------------------------------------------------------------

% uncertainty initial condition - Gaussian density
p_bounds = [ 0        10    	            % p1
	         0        10                % p2
			 0        10                % p3
	         1        0.5               % sigma_model
	         1        0.5];			    % l

% uncertainty measurements: 3*std = err_perc (error normally distributed)
err_perc = 1/100;       % +/- 5% sampling error
meas_std = err_perc;
R = meas_std^2; 

%% ------------------------------------------------------------------------
% Time settings
%--------------------------------------------------------------------------
num_stages = 20;

x1_span = linspace(0, 1, 11);
x2_span = linspace(0, 1, 11);
[X1_grid, X2_grid] = meshgrid(x1_span, x2_span);

num_designs = numel(X1_grid);
for i = 1:num_designs
    design(i).x1 = X1_grid(i);
    design(i).x2 = X2_grid(i);
end;

%design = tspan;

%% ------------------------------------------------------------------------
% Filter settings
%--------------------------------------------------------------------------

init_filter.no_particles = 5000;           % no particles         

%% ------------------------------------------------------------------------
% Number of trials
%--------------------------------------------------------------------------

num_trials = 1;

%% ------------------------------------------------------------------------
% For instant prediction
%--------------------------------------------------------------------------
num_prediction = 11^2;
prediction_x = 1:num_prediction;
prediction_x1_span = linspace(0, 1, 100);
prediction_x2_span = linspace(0, 1, 100);
[prediction_X1_grid, prediction_X2_grid] = meshgrid(prediction_x1_span, prediction_x2_span);
for i = 1:num_prediction
    prediction_design(i).x1 = prediction_X1_grid(i);
    prediction_design(i).x2 = prediction_X2_grid(i);
end;
lowerbound = zeros(1,size(prediction_x,2));
upperbound = zeros(1,size(prediction_x,2));

