function kK = kernVecOrMat_SE( scnLeft, scnRight, sigma_GP, covlen_GP, design_Left, design_Right, sigma_meas )

nLeft = length( scnLeft );
nRight = length( scnRight );

if min(nLeft, nRight) == 0
    kK = 0;
    return;
end;

kK = zeros( nLeft, nRight );

for i = 1 : nLeft
    for j = 1 : nRight
    
        r1 = (design_Left(scnLeft(i)).x - design_Right(scnRight(j)).x)^2; 
        r2 = (design_Left(scnLeft(i)).y - design_Right(scnRight(j)).y)^2;
        kK(i,j) = sigma_GP^2 * exp( -1/2*(r1/covlen_GP(1)^2) )* exp( -1/2*(r2/covlen_GP(2)^2) );
        if( r1 == 0&& r2 == 0)
            kK(i,j) = kK(i,j) + sigma_meas;
        end;
        
    end;
end;