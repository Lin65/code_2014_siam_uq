function kl = knnKL_GaussApprox(prior, poster, dim)

	mean_post = mean( poster, 2);
	mean_prior = mean( prior, 2);
	Sigma_post = cov( poster' );
	Sigma_prior = cov( prior' );
		
	kl = 1/2*( trace(inv(Sigma_prior)*Sigma_post) + (mean_prior-mean_post)'*inv(Sigma_prior)*(mean_prior-mean_post)- log( det(Sigma_post)/det(Sigma_prior) ) - dim );
