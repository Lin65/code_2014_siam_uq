function posterior = stan_MCMCupdateGP(p_bounds, x_sofar, obs_sofar, design, R, no_smps)

vec_data1 = zeros(numel(x_sofar),1);
vec_data2 = zeros(numel(x_sofar),1);

for i = 1:numel(x_sofar)
    vec_data1(i) = design(x_sofar(i)).x;
    vec_data2(i) = design(x_sofar(i)).y;
end;

data.x1data = vec_data1'; 
data.x2data = vec_data2';
data.ydata = obs_sofar; 


Hartman_dat = struct('N',numel(x_sofar),'x',data.x1data,'y',data.x2data,'d',data.ydata);

if(numel(x_sofar)==1)
   fit1 = stan('file','first_Hartman.stan','data',Hartman_dat,'iter',2000,'chains',4,'seed',1);
   fit1.block();
   theta1 = fit1.extract.theta1;
   theta2 = fit1.extract.theta2;
   theta3 = fit1.extract.theta3;
   theta4 = fit1.extract.theta4;
   theta5 = fit1.extract.theta5;
   theta6 = fit1.extract.theta6;
else 
   fit = stan('file','HartmanModel.stan','data',Hartman_dat,'iter',2000,'chains',4,'seed',1);
   fit.block();
   theta1 = fit.extract.theta1;
   theta2 = fit.extract.theta2;
   theta3 = fit.extract.theta3;
   theta4 = fit.extract.theta4;
   theta5 = fit.extract.theta5;
   theta6 = fit.extract.theta6;
end;

samples = [theta1';theta2'; theta3'; theta4';theta5';theta6'];

posterior = samples;

