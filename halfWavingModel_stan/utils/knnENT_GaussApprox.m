function ent = knnENT_GaussApprox(my_chain)

[lenChain, dimChain] = size(my_chain);
my_d = dimChain;

if( my_d ~= dimChain )
    error('the joint dimension in the split is different from the dimension of the chain')
end;

%--------------------------------------------------------------------------
% Normalize the chain
%--------------------------------------------------------------------------
my_Sigma = cov(my_chain);

%--------------------------------------------------------------------------
% Estimate mutual information
%--------------------------------------------------------------------------


exactENT = log( sqrt( (2*pi*exp(1))^my_d * det(my_Sigma) ) );



ent = exactENT;