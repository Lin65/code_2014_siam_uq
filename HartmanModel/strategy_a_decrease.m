%%---------------------------------------------
%  hybrid: a is decreasing 
%%---------------------------------------------


clear all;
close all;
clc;

addpath('utils');
paths=[pwd,'\data_figures\strategy_6\'];

%% ------------------------------------------------------------------------
% Strategy number
%--------------------------------------------------------------------------

no_strategy = 6;

%% ------------------------------------------------------------------------
% Load measurements
% Get global data by running "main_model.m"
%--------------------------------------------------------------------------


load measurement_Hartman.mat;

main_model_Hartman;
 
%% ------------------------------------------------------------------------
% strategy setting
%--------------------------------------------------------------------------

hybrid_a = zeros(1,num_stages);
for i = 1:num_stages
    hybrid_a(i) = 10  - 10*i/num_stages;
end;


%% ------------------------------------------------------------------------
% Main Simulation
%--------------------------------------------------------------------------

trials_Post_entropy = zeros(1, num_stages);
trials_Post_entropy3 = zeros(1, num_stages);
trials_Post_mean_modelerror = zeros(1, num_stages);
trials_Post_std_modelerror = zeros(1, num_stages);
trials_Post_mean_correlationlength = zeros(1, num_stages);
trials_Post_std_correlationlength = zeros(1, num_stages);
trials_KL = zeros(1, num_stages);
trials_KL3 = zeros(1, num_stages);
trials_MI_est = zeros(1, num_stages);

MI_est = zeros(1, num_designs);
min_distance = zeros(1, num_designs);
%hybrid_distance = zeros(1, num_designs);
my_state = zeros(1,init_filter.no_particles);
my_chain_data = zeros( init_filter.no_particles,1 );  

indexMI = zeros(1,num_stages);

eval_error = zeros(1, num_stages);
eval_std = zeros(1, num_stages);
area_metric = zeros(1, num_stages);
area_metric_new = zeros(1, num_stages);

update_samples = cell(1, num_stages);
instant_prediction = cell(1, num_stages);

    fprintf( '   |- Process trial %d / %d\n', 1, 1);
    
    p_pf = repmat( p_bounds(:,1), 1, init_filter.no_particles ) + repmat( p_bounds(:,2), 1, init_filter.no_particles ) .* randn( 5, init_filter.no_particles );
    p_pf_prop = p_pf;

    % initializations
    MI_est(:) = 0;
	options = 1:num_designs;
% 	figure;
    for k_stage = 1 : num_stages	    
        
		MI_est(:) = 0;
		min_distance(:) = 0;
		%hybrid_distance(:) = 0;
            Am = zeros(1,size(prediction_x,2));
			    Am_new = zeros(1,size(prediction_x,2));


		
    	for k_design = options
		    fprintf( '   |- Process design %d / %d\n', k_design, num_designs );		  	
		    % propagate all the particles to corresponding time period
            	rand('seed',101);
    randn('seed',101);
			if (k_stage == 1)
		        for i = 1 : init_filter.no_particles		
		            my_state(i) = p_pf_prop(1,i) + p_pf_prop(2,i)*design(k_design).x1 + p_pf_prop(3,i)*design(k_design).x2 + simulateMeas( 0, p_pf_prop(4,i) );
			        my_chain_data(i,:) = simulateMeas( my_state(i), meas_std );
		        end;
		    end;			
			% GP prediction
			if (k_stage > 1)			
			    training_x = x_sofar;
			    training_obs = obs_sofar;
			    test_x = k_design;
                test_design = design;
                	rand('seed',101);
    randn('seed',101);
			    my_chain_data = GPprediction(p_pf_prop, training_x, training_obs, test_x, design, test_design, R,1);				
				options_used = indexMI(1,1:(k_stage-1));			
			    min_distance( k_design) = mindistance( options_used, k_design, design);
			end;
		
		    my_chain = [my_chain_data,p_pf_prop(:,:)'];                          % particles of both states and observations			
	        MI_est( k_design ) = knnMI_GaussApprox( my_chain, [1,5]);
          			
	    end;
		if (k_stage == 1)
		    hybrid_distance = MI_est/max(MI_est);
		else
		    hybrid_distance = MI_est/max(MI_est) + hybrid_a(k_stage) * min_distance/max(min_distance);
		end;
		
		[max_distance, indMax] = max(hybrid_distance);		
			
	    % Bayes update - get also the posterior
	    fprintf( '      |- Bayesian update\n' );		
		indexMI(1,k_stage) = indMax
		
        % MCMC update 		
		index_obs = indexMI(1,1:k_stage);
		x_sofar = index_obs;
		obs_sofar = meas(1,index_obs);
			rand('seed',101);
    randn('seed',101);
        P_pf_updateRaw = MCMCupdateGP (p_bounds, x_sofar, obs_sofar, design, R, init_filter.no_particles+500);
        P_pf_update = P_pf_updateRaw(:,501:end);
		update_samples{k_stage} = P_pf_update;
		% plot prediction
        	rand('seed',101);
    randn('seed',101);
		instant_prediction{k_stage} = GPprediction(P_pf_update, x_sofar, obs_sofar, prediction_x, design, prediction_design, R,0);
    for i =1:size(prediction_x,2)
        [CDF,Points] = ecdf(instant_prediction{k_stage}(:,i));
        points_uniform = linspace(min(Points),max(Points),init_filter.no_particles);
        for j = 1:init_filter.no_particles
            cdf_new(j) = CDF(max(find(Points<=points_uniform(j))));
        end;
             index1_new = find(points_uniform<meas(i));
            index2_new = find(points_uniform>meas(i));
            if (numel(index1_new)<2)
                area1_new = 0;
            else 
                area1_new = trapz(points_uniform(index1_new),cdf_new(index1_new));
            end;
            
            if (numel(index2_new)<2)
                area2_new = 0;
            else
                area2_new = trapz(points_uniform(index2_new),1 - cdf_new(index2_new));
            end;
            
            Am_new(i) = area1_new + area2_new;
            
            index1 = find(Points<meas(i));
            index2 = find(Points>meas(i));
            if (numel(index1)<2)
                area1 = 0;
            else 
                area1 = trapz(Points(index1),CDF(index1));
            end;
            
            if (numel(index2)<2)
                area2 = 0;
            else
                area2 = trapz(Points(index2),1 - CDF(index2));
            end;
            
            Am(i) = area1 + area2;
            lowerbound(prediction_x(i)) = Points(max(find(CDF<0.025))+1);
        upperbound(prediction_x(i)) = Points(min(find(CDF>0.975))-1);
    end;
    Am_new(x_sofar)=0;    
	area_metric(k_stage) = sum(Am)/size(prediction_x,2);
    area_metric_new(k_stage) = (sum(Am_new) - sum(Am_new(x_sofar)))/size(prediction_x,2);
	
        pred_mean = mean(instant_prediction{k_stage});
        pred_std =  std(instant_prediction{k_stage});
        eval_error(k_stage) = sum(abs(pred_mean - truth_prediction'))/numel(pred_mean);
		eval_std(k_stage) =  sum(pred_std)/numel(pred_mean);         
%         prediction_lowerbound = reshape( lowerbound, size(prediction_X1_grid,1), size(prediction_X1_grid,2) );
%         prediction_upperbound = reshape( upperbound, size(prediction_X1_grid,1), size(prediction_X1_grid,2) );
%  		
%         figure;       
%         surf(prediction_X1_grid,prediction_X2_grid,prediction_lowerbound);
%         hold on;
%          surf(prediction_X1_grid,prediction_X2_grid,prediction_upperbound);
% %         X = [prediction_tspan, fliplr(prediction_tspan)];
% %         Y = [upperbound, fliplr(lowerbound)];
% %         fill(X,Y,'c');
% %         hold on;
% %         plot(tspan,truth,'r');
%  		title({'hybrid a is decreasing: instant prediction at stage ',num2str(k_stage)});
% %         set(gca,'XTick',tspan); 
%         xlabel('x1');
%         ylabel('x2');
%         zlabel('y');
%  		saveas(gcf,[paths,'fig',num2str(k_stage),'.jpg']);		


		p_pf_update = P_pf_update;
	    trials_Post_entropy(1,k_stage) = knnENT_GaussApprox( p_pf_update');	
        trials_Post_entropy3(1,k_stage) = knnENT_GaussApprox( p_pf_update(1:3,:)');	
	    trials_MI_est(1,k_stage) = MI_est(indMax);                	
	    trials_Post_mean_modelerror(1, k_stage) = mean(p_pf_update(4,:)');
        trials_Post_std_modelerror(1, k_stage) = std(p_pf_update(4,:)');
        trials_Post_mean_correlationlength(1, k_stage) = mean(p_pf_update(5,:)');
        trials_Post_std_correlationlength(1, k_stage)= std(p_pf_update(5,:)');
        trials_KL3(1, k_stage) = knnKL_GaussApprox(p_pf_prop(1:3,:),p_pf_update(1:3,:),3);
		trials_KL(1, k_stage) = knnKL_GaussApprox(p_pf_prop,p_pf_update,5);
		p_pf_prop = p_pf_update;
		options(find(options == indMax)) = [];
		
    end;
	
% 	saveas(gcf,[paths,'fig',num2str(k_stage),'.eps']);		



    mean_MI_est = trials_MI_est; 
	mean_Post_entropy = trials_Post_entropy;
    mean_Post_entropy3 = trials_Post_entropy3;   
    mean_KL = trials_KL;
	mean_KL3 = trials_KL3;
   	mean_Post_mean_modelerror = trials_Post_mean_modelerror;
    mean_Post_std_modelerror = trials_Post_std_modelerror;
    mean_Post_mean_correlationlength = trials_Post_mean_correlationlength;
    mean_Post_std_correlationlength = trials_Post_std_correlationlength;

figure;
plot(mean_Post_entropy);
title('post entropy');
saveas(gcf,[paths,'post entropy of all.jpg']);
saveas(gcf,[paths,'post entropy of all.eps']);

     figure;
     plot(mean_Post_entropy3);
     title('post entropy 3');
	     saveas(gcf,[paths,'post entropy 3.jpg']);
    saveas(gcf,[paths,'post entropy 3.eps']);
	
     figure;
     plot(mean_KL);
     title('KL');	 
    saveas(gcf,[paths,'KL.jpg']);
    saveas(gcf,[paths,'KL.eps']);
	
     figure;
     plot(mean_KL3);
     title('KL3');
    saveas(gcf,[paths,'KL1.jpg']);
    saveas(gcf,[paths,'KL1.eps']);
	
    figure;
    plot(eval_error);
    title('performance measurements : prediction error');
saveas(gcf,[paths,'prediction error.jpg']);
saveas(gcf,[paths,'prediction error.eps']);

    figure;
    plot(eval_std);
   title('performance measurements : prediction std');
saveas(gcf,[paths,'prediction std.jpg']);
saveas(gcf,[paths,'prediction std.eps']);

figure;
plot(area_metric_new);
title('area metric');
saveas(gcf,[paths,'area metric.jpg']);
saveas(gcf,[paths,'area metric.eps']);
        


save(['data_figures/results_strategy_' num2str(no_strategy) '.mat']);