clear all;
close all;
clc;


mainsetting;
my_paths='data_figures';

% truth----the whole domain
truth = Realpuff(X_truth_grid, Y_truth_grid);

% truth_meas----the meas domain
truth_meas = Realpuff(X_grid, Y_grid);

meas_grid = truth_meas + meas_std*randn(size(truth_meas,1),size(truth_meas,2));
meas = reshape(meas_grid, prod(size(meas_grid)), 1)';

% truth_prediction---- the prediction domain
truth_prediction_grid = Realpuff(prediction_X_grid, prediction_Y_grid); 
truth_prediction = reshape(truth_prediction_grid, prod(size(truth_prediction_grid)), 1)';

figure;
surf(X_truth_grid, Y_truth_grid, truth);hold on;
plot3(X_grid, Y_grid, meas_grid,'.y','MarkerSize',20);
plot3(prediction_X_grid, prediction_Y_grid, truth_prediction_grid,'.r','MarkerSize',20);

xlabel('x');
ylabel('y');
zlabel('c');
title('puff model: truth and measurements');



%theta = [theta0, theta1, theta2];
theta = [1 1 1];

sim_plot = Simpuff(prediction_X_grid, prediction_Y_grid, theta); 

% we want each strategy has the same initial samples
 rand('seed',1001);
 randn('seed',1001); 
 p_pf_prop3 = repmat( p_bounds(1:3,1), 1, init_filter.no_particles ) + repmat( p_bounds(1:3,2), 1, init_filter.no_particles ) .* randn( 3, init_filter.no_particles );
 p_pf_prop6 = repmat( p_bounds(4:6,1), 1, init_filter.no_particles ) + repmat( p_bounds(4:6,2), 1, init_filter.no_particles ) .* randn( 3, init_filter.no_particles );
 p_pf_prop6 = abs(p_pf_prop6) + 0.0000001;
 p_pf_prop = [p_pf_prop3;p_pf_prop6];
 p_pf_initial = p_pf_prop;	
 
% figure;
% surf(prediction_X_grid, prediction_T_grid, truth_prediction);
% 
% truth_prediction = reshape(truth_plot, numel(truth_plot),1);
% 
% hold on;
% %plot3(X_grid, Y_grid, meas_grid,'.r','MarkerSize',5);
% % legend('truth','measurements');
% 
% xlabel('x');
% ylabel('y');
% zlabel('c');
% 
% 
% 
% title('puff model: truth and measurements');
% 
% saveas(gcf,[my_paths,'\main_results\truth.jpg']); 

figure;
surf(prediction_X_grid, prediction_Y_grid, truth_prediction_grid - sim_plot);
xlabel('x');
ylabel('y');
zlabel('c');
title('waving model: model discrepency \theta = [1 1 1]');

saveas(gcf,[my_paths,'\main_results\discrepency.jpg']); 



save measurement.mat;
