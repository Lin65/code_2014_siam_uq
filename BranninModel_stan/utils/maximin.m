function MaximinList = maximin(list, firstElement,design)


MaximinList = zeros(1,size(list,2));
MaximinList(1) = firstElement;

leftList = list;
leftList(find(leftList == firstElement)) = [];

Listlength = size(leftList,2);

for i = 1:(Listlength - 1)
    no = maximindistance(MaximinList(1:i),leftList,design);
	
	MaximinList(i+1) = leftList(no);
	leftList(no) = [];
end;

MaximinList(Listlength+1) = leftList(1);


	
    



