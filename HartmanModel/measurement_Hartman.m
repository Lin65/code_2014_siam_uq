clear all;
close all;
clc;


main_model_Hartman;
my_paths='data_figures';

a = [10 3 17 3.5 1.7 8;0.05 10 17 0.1 8 14; 3 3.5 1.7 10 17 8; 17 8 0.05 10 0.1 14];
c = [1 1.2 3 3.2];
p = [0.1312 0.1696 0.5569 0.0124 0.8283 0.5886; 0.2329 0.4135 0.8307 0.3736 0.1004 0.9991; 0.2348 0.1451 0.3522 0.2883 0.3047 0.6650; 0.4047 0.8828 0.8732 0.5743 0.1091 0.0381];

truth = zeros(size(X1_grid,1),size(X1_grid,2));

for i =1:4
    truth = truth -c(i)*exp(-a(1,1)*(X1_grid - p(1,1)).^2 -a(1,2)*(X2_grid - p(1,2)).^2 - a(1,3)*(0.5 - p(1,3))^2 - a(1,4)*(0.5 - p(1,4))^2 - a(1,5)*(0.5 - p(1,5))^2 -a(1,6)*(0.5 - p(1,6))^2);
end;

	    

meas_grid = truth + meas_std*randn(size(truth,1),size(truth,2));
meas = reshape(meas_grid, prod(size(meas_grid)), 1)';

figure;
% plot truth and meas
truth_plot = truth;% (prediction_X2_grid - 5.1*(prediction_X1_grid/(2*pi)).^2 + 5*prediction_X1_grid/pi -6).^2 + 10*(1 - 1/(8*pi)).*cos(prediction_X1_grid) + 10; 
surf(prediction_X1_grid, prediction_X2_grid, truth_plot);

truth_prediction = reshape(truth_plot, numel(truth_plot),1);

hold on;
plot3(X1_grid, X2_grid, meas_grid,'.r','MarkerSize',5);
% legend('truth','measurements');

xlabel('x1');
ylabel('x2');
zlabel('y');



title('Hartman model: truth and measurements');

saveas(gcf,[my_paths,'\main_results\truth.jpg']); 

save measurement_Hartman.mat;