function dist = mindistance(oldindex, new, designList)


m = size(oldindex,2);

mindistance = inf;
for j = 1:m
    distance = sqrt((designList(new).x1 - designList(oldindex(j)).x1)^2 + (designList(new).x2 - designList(oldindex(j)).x2)^2);
    if distance < mindistance	
       mindistance = distance;
    end;
end;
    
dist = mindistance;
	

