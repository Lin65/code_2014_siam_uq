function prediction_data = GPprediction(p_pf_prop, training_x_index, training_y, test_x_index, design, test_design, R , obs_sign)

training_x = zeros(2, numel(training_x_index));
test_x = zeros(2,numel(test_x_index));

for i = 1:numel(training_x_index)
    training_x(1,i) = design(training_x_index(i)).x;
    training_x(2,i) = design(training_x_index(i)).y;
end;

for i = 1:numel(test_x_index)
    test_x(1,i) = test_design(test_x_index(i)).x;
    test_x(2,i) = test_design(test_x_index(i)).y;
end;

no_smps = size(p_pf_prop, 2);

prediction_data = zeros(no_smps,numel(test_x_index));

mean_test = zeros(1,size(test_x,2));
mean_train = zeros(1,size(training_x,2));

for i = 1:no_smps
  %  mean_test = (repmat(p_pf_prop(1,i),size(test_x,2),1) + repmat(p_pf_prop(2,i),size(test_x,2),1).*test_x(1,:)' + repmat(p_pf_prop(3,i),size(test_x,2),1).*test_x(2,:)')' ;
	
    for j = 1:size(test_x,2)
        mean_test(j) = Simpuff(test_x(1,j), test_x(2,j), p_pf_prop(1:3,i));
    end;
    
 
    
 %   mean_train = (repmat(p_pf_prop(1,i),size(training_x,2),1) + repmat(p_pf_prop(2,i),size(training_x,2),1).*training_x(1,:)' + repmat(p_pf_prop(3,i),size(training_x,2),1).*training_x(2,:)')';

    for j = 1:size(training_x,2)
        mean_train(j) = Simpuff(training_x(1,j), training_x(2,j), p_pf_prop(1:3,i));
    end;
    
 
    
    KK_train = kernVecOrMat_SE( training_x_index', training_x_index', p_pf_prop(4,i), p_pf_prop(5:6,i),design, design,R);
	if obs_sign ==0
       KK_test = kernVecOrMat_SE( test_x_index', test_x_index', p_pf_prop(4,i), p_pf_prop(5:6,i),test_design, test_design, 0);
	else
       KK_test = kernVecOrMat_SE( test_x_index', test_x_index', p_pf_prop(4,i), p_pf_prop(5:6,i),test_design, test_design, R);
	end;
	   
    KK_test_train = kernVecOrMat_SE( test_x_index', training_x_index', p_pf_prop(4,i), p_pf_prop(5:6,i),test_design,design, 0);
   
    mean_predict = mean_test + (KK_test_train*inv(KK_train)*(training_y' - mean_train'))';
    
    eps_tresh = 1000000*eps;

    Rt = chol(KK_train + eye(size(KK_train))*eps_tresh);
    b = Rt'\KK_test_train';
   % cov_test = KK_test - b'*b;
    
%      p_pf_prop(4,i)
%     p_pf_prop(5,i)
%     detm = det(KK_test)

% compute using a sequence of rank-one updates: sig = K_test - v'*v;
S_test = chol(KK_test + eye(size(KK_test))*eps_tresh);
S_sig = S_test;
for j = 1 : size(b,1)
	S_sig = cholupdate(S_sig,b(j,:)','-');
end;

% generate covariance matrix
cov_test = S_sig'*S_sig;
%     cov_test = KK_test - KK_test_train*inv(KK_train)*KK_test_train';
%    % Std_predict(i,:) = sqrt(diag(cov_test));
%     cov_test =  cov_test + diag(abs(diag( cov_test))*0.0001 + 0.000001);

    prediction_data(i,:) = mean_predict +(chol(cov_test)'*randn(size(test_x,2),1))';
end;
