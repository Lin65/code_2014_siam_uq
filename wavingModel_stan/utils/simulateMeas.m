function sim_meas = simulateMeas( cur_state, meas_std )

err_norm = randn(size(cur_state));			

% meas = true + % * true, where "x" normally distributed
sim_meas = cur_state + meas_std * err_norm;