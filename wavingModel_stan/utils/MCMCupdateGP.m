function posterior = MCMCupdateGP(p_bounds, x_sofar, obs_sofar, design, R, no_smps)

vec_data1 = zeros(numel(x_sofar),1);
vec_data2 = zeros(numel(x_sofar),1);

for i = 1:numel(x_sofar)
    vec_data1(i) = design(x_sofar(i)).x;
    vec_data2(i) = design(x_sofar(i)).y;
end;

data.x1data = vec_data1; 
data.x2data = vec_data2;
data.ydata = obs_sofar'; 


prifun = @(par,pri_mu,pri_sig) (par - pri_mu)/diag(pri_sig.^2)*(par - pri_mu)';
modelfun = @(x1,x2,theta) Simpuff(x1, x2, theta(1));
ssfun    = @(theta,data) (data.ydata-modelfun(data.x1data,data.x2data,theta))'/covfun(theta,data.x1data,data.x2data,R)* (data.ydata-modelfun(data.x1data,data.x2data,theta)) + 2*log(det(covfun(theta,data.x1data,data.x2data,R)));

params = {
    {'theta1', p_bounds(1,1), 0, 100, p_bounds(1,1), p_bounds(1,2)}
    {'theta2', p_bounds(2,1), 0.00000000001, 100, p_bounds(2,1), p_bounds(2,2)}
    {'theta3', p_bounds(3,1), 0.00000000001, 100, p_bounds(3,1), p_bounds(3,2)}
    {'theta4', p_bounds(4,1), 0.00000000001, 100, p_bounds(4,1), p_bounds(4,2)}
    };

model.ssfun  = ssfun;
model.priorfun = prifun;


options.nsimu = no_smps;

[res,chain,s2chain] = mcmcrun(model,data,params,options);

posterior = chain';

clear data model options
