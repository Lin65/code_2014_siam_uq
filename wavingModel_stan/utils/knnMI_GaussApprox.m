function mi = knnMI_GaussApprox(my_chain,my_split)

[lenChain, dimChain] = size(my_chain);
my_dx = my_split(1);
my_dy = my_split(2);
my_d = sum(my_split);

if( my_d ~= dimChain )
    error('the joint dimension in the split is different from the dimension of the chain')
end;

%--------------------------------------------------------------------------
% Normalize the chain
%--------------------------------------------------------------------------
my_Sigma = cov(my_chain);

%--------------------------------------------------------------------------
% Estimate mutual information
%--------------------------------------------------------------------------
my_Sigma_x = my_Sigma(1:my_dx,1:my_dx);
my_Sigma_y = my_Sigma(my_dx+1:end,my_dx+1:end);
exactENT_x = log( sqrt( (2*pi*exp(1))^my_dx * det(my_Sigma_x) ) );
exactENT_y = log( sqrt( (2*pi*exp(1))^my_dy * det(my_Sigma_y) ) );
exactENT_xy = log( sqrt( (2*pi*exp(1))^my_d * det(my_Sigma) ) );
%%%%%%%%%%modified by Xiaofan Wang
% exactENT_x = log( sqrt( (2*pi*exp(1))^my_dx *abs(det(my_Sigma_x) )) );
% exactENT_y = log( sqrt( (2*pi*exp(1))^my_dy * abs(det(my_Sigma_y)) ) );
% exactENT_xy = log( sqrt( (2*pi*exp(1))^my_d *abs( det(my_Sigma) )) );

exactMI = exactENT_x + exactENT_y - exactENT_xy;

mi = exactMI;