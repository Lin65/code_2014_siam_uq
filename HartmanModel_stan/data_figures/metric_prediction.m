clear all;
close all;
clc;

load results_strategy_1.mat

prediction_1 = instant_prediction;

truth = truth_prediction;

for i = 1: num_stages
    for j = 1:num_prediction
        mean_prediction_1(i,j) = mean(prediction_1{1,i}(:,j));
        std_prediction_1(i,j) = std(prediction_1{1,i}(:,j));
        error_1(i,j) = abs(mean_prediction_1(i,j) - truth_prediction(j));
        std_pre_1(i,j) = std_prediction_1(i,j);
        metric_1(i,j) = error_1(i,j)/std_pre_1(i,j);
        pval(i,j) = normcdf(truth_prediction(j),mean_prediction_1(i,j),std_prediction_1(i,j));
        pval_1(i,j) = min(pval(i,j), 1-pval(i,j));
    end;
    mean_pval_1(i) = mean(pval_1(i,:));
    mean_error_1(i) = mean(error_1(i,:));
    mean_std_1(i) = mean(std_pre_1(i,:));
    mean_metric_1(i) = mean(metric_1(i,:));
end;
index_1 = x_sofar;
%--------------------------------------------------
load results_strategy_2.mat

prediction_2 = instant_prediction;

truth = truth_prediction;

for i = 1: num_stages
    for j = 1:num_prediction
        mean_prediction_2(i,j) = mean(prediction_2{1,i}(:,j));
        std_prediction_2(i,j) = std(prediction_2{1,i}(:,j));
        error_2(i,j) = abs(mean_prediction_2(i,j) - truth_prediction(j));
        std_pre_2(i,j) = std_prediction_2(i,j);
        metric_2(i,j) = error_2(i,j)/std_pre_2(i,j);
        pval(i,j) = normcdf(truth_prediction(j),mean_prediction_2(i,j),std_prediction_2(i,j));
        pval_2(i,j) = min(pval(i,j), 1-pval(i,j));
    end;
    mean_pval_2(i) = mean(pval_2(i,:));
    mean_error_2(i) = mean(error_2(i,:));
    mean_std_2(i) = mean(std_pre_2(i,:));
    mean_metric_2(i) = mean(metric_2(i,:));
end;
index_2 = x_sofar;

%-------------------------------------------------------------
%--------------------------------------------------
% load results_strategy_4.mat
% 
% prediction_4 = instant_prediction;
% 
% truth = truth_prediction;
% 
% for i = 1: num_stages
%     mean_prediction_4(i) = mean(prediction_4{i});
%     std_prediction_4(i) = std(prediction_4{i});
%     error_4(i) = abs(mean_prediction_4(i) - truth);
%     std_pre_4(i) = std_prediction_4(i);
%     metric_4(i) = error_4(i)/std_pre_4(i);
%     pval = normcdf(truth,mean_prediction_4(i),std_prediction_4(i));
%     pval_4(i) = min(pval, 1-pval);  
% end;
% index_4 = x_sofar;

% %--------------------------------------------------
% load results_strategy_3.mat
% 
% prediction_3 = instant_prediction;
% 
% truth = truth_prediction;
% 
% for i = 1: num_stages
%     mean_prediction_3(i) = mean(prediction_3{i});
%     std_prediction_3(i) = std(prediction_3{i});
%     error_3(i) = abs(mean_prediction_3(i) - truth);
%     std_pre_3(i) = std_prediction_3(i);
%     metric_3(i) = error_3(i)/std_pre_3(i);
%     pval = normcdf(truth,mean_prediction_3(i),std_prediction_3(i));
%     pval_3(i) = min(pval, 1-pval);  
% end;
% index_3 = x_sofar;
% 
% %--------------------------------------------------
% load results_strategy_6.mat
% 
% prediction_6 = instant_prediction;
% 
% truth = truth_prediction;
% 
% for i = 1: num_stages
%     mean_prediction_6(i) = mean(prediction_6{i});
%     std_prediction_6(i) = std(prediction_6{i});
%     error_6(i) = abs(mean_prediction_6(i) - truth);
%     std_pre_6(i) = std_prediction_6(i);
%     metric_6(i) = error_6(i)/std_pre_6(i);
%     pval = normcdf(truth,mean_prediction_6(i),std_prediction_6(i));
%     pval_6(i) = min(pval, 1-pval);  
% end;
% index_6 = x_sofar;
% 
%--------------------------------------------------
load results_strategy_7.mat

prediction_7 = instant_prediction;

truth = truth_prediction;

for i = 1: num_stages
    for j = 1:num_prediction
        mean_prediction_7(i,j) = mean(prediction_7{1,i}(:,j));
        std_prediction_7(i,j) = std(prediction_7{1,i}(:,j));
        error_7(i,j) = abs(mean_prediction_7(i,j) - truth_prediction(j));
        std_pre_7(i,j) = std_prediction_7(i,j);
        metric_7(i,j) = error_7(i,j)/std_pre_7(i,j);
        pval(i,j) = normcdf(truth_prediction(j),mean_prediction_7(i,j),std_prediction_7(i,j));
        pval_7(i,j) = min(pval(i,j), 1-pval(i,j));
    end;
    mean_pval_7(i) = mean(pval_7(i,:));
    mean_error_7(i) = mean(error_7(i,:));
    mean_std_7(i) = mean(std_pre_7(i,:));
    mean_metric_7(i) = mean(metric_7(i,:));
end;
index_7 = x_sofar;


%--------------------------------------------------------------------------------
load results_strategy_8.mat

prediction_8 = instant_prediction;

truth = truth_prediction;

% mean_prediction_8 = zeros(num_stages, num_prediction);
% std_prediction_8 = zeros(num_stages, num_prediction);
% error_8 = zeros(num_stages, num_prediction);
% std_pre_8 = zeros(num_stages, num_prediction);
% metric_8 = zeros(num_stages, num_prediction);
% pval = zeros(num_stages, num_prediction);
% pval_8 = zeros(num_stages, num_prediction);

for i = 1: num_stages
    for j = 1:num_prediction
        mean_prediction_8(i,j) = mean(prediction_8{1,i}(:,j));
        std_prediction_8(i,j) = std(prediction_8{1,i}(:,j));
        error_8(i,j) = abs(mean_prediction_8(i,j) - truth_prediction(j));
        std_pre_8(i,j) = std_prediction_8(i,j);
        metric_8(i,j) = error_8(i,j)/std_pre_8(i,j);
        pval(i,j) = normcdf(truth_prediction(j),mean_prediction_8(i,j),std_prediction_8(i,j));
        pval_8(i,j) = min(pval(i,j), 1-pval(i,j));
    end;
    mean_pval_8(i) = mean(pval_8(i,:));
    mean_error_8(i) = mean(error_8(i,:));
    mean_std_8(i) = mean(std_pre_8(i,:));
    mean_metric_8(i) = mean(metric_8(i,:));
end;
index_8 = x_sofar;


figure;
plot(mean_pval_1);
hold on;
plot(mean_pval_2,'r');
%plot(pval_4,'y');
%plot(pval_3,'c');
%plot(pval_6,'m*');
plot(mean_pval_7,'k--');
plot(mean_pval_8,'g*');
%legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('new metric'); 
set(gca,'XTick',[1:1:num_stages]); 
title('pval ');


figure;
plot(mean_error_1);
hold on;
plot(mean_error_2,'r');
%plot(error_4,'y');
%plot(error_3,'c');
%plot(error_6,'m*');
plot(mean_error_7,'k--');
plot(mean_error_8,'g*');
%legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('new metric'); 
set(gca,'XTick',[1:1:num_stages]); 
title('error');


figure;
plot(mean_std_1);
hold on;
plot(mean_std_2,'r');
%plot(std_pre_4,'y');
%plot(std_pre_3,'c');
%plot(std_pre_6,'m*');
plot(mean_std_7,'k--');
plot(mean_std_8,'g*');
%legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('new metric'); 
set(gca,'XTick',[1:1:num_stages]); 
title('std');


figure;
plot(mean_metric_1);
hold on;
plot(mean_metric_2,'r');
%plot(mean_metric_4,'y');
%plot(metric_3,'c');
%plot(metric_6,'m*');
plot(mean_metric_7,'k--');
plot(mean_metric_8,'g*');
%legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
xlabel('stage');% x???
ylabel('new metric'); 
set(gca,'XTick',[1:1:num_stages]); 
title('metric');

% 
% 
% 
% figure;
% plot(pval_1(:,1)+pval_1(:,2)+pval_1(:,3)+pval_1(:,4));
% hold on;
% plot(pval_2(:,1)+pval_2(:,2)+pval_2(:,3)+pval_2(:,4),'r');
% plot(pval_4(:,1)+pval_4(:,2)+pval_4(:,3)+pval_4(:,4),'y');
% plot(pval_5(:,1)+pval_5(:,2)+pval_5(:,3)+pval_5(:,4),'c');
% plot(pval_6(:,1)+pval_6(:,2)+pval_6(:,3)+pval_6(:,4),'m*');
% plot(pval_7(:,1)+pval_7(:,2)+pval_7(:,3)+pval_7(:,4),'k');
% plot(pval_8(:,1)+pval_8(:,2)+pval_8(:,3)+pval_8(:,4),'--');
% legend('maximin','MI','hybrid: \alpha = 1','hybrid: segmentation','hybrid: \alpha decreases','hybrid: adaptive entropy','hybrid: adaptive KL');
% xlabel('stage');% x???
% ylabel('new metric'); 
% set(gca,'XTick',[1:1:num_stages]); 
% title('sum of pval of 4 parameters');

% for box plot
% for i = 1:num_stages
%     prediction_stage{i} = [prediction_1{i}';prediction_2{i}';prediction_4{i}';prediction_5{i}';prediction_6{i}';prediction_7{i}';prediction_8{i}']';
% end;
% 
% figure;
% 
% subplot(5,1,1)
% boxplot(prediction_stage{1}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,2)
% boxplot(prediction_stage{2}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,3)
% boxplot(prediction_stage{3}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,4)
% boxplot(prediction_stage{4}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,5)
% boxplot(prediction_stage{5}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% figure;
% subplot(5,1,1)
% boxplot(prediction_stage{6}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,2)
% boxplot(prediction_stage{7}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,3)
% boxplot(prediction_stage{8}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,4)
% boxplot(prediction_stage{9}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
% subplot(5,1,5)
% boxplot(prediction_stage{10}(:,:));hold on;
% plot([0 8],[truth truth],'g--');
% 
