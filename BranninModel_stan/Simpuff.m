function z = Simpuff( x, y, theta )


theta0 = theta(1);
theta1 = theta(2);
theta2 = theta(3);

z = theta0 + theta1*x + theta2*y;

end




