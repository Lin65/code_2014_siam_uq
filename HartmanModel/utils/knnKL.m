function kl = knnKL(my_chain1, my_chain2, my_knn)

[lenChain1, dimChain1] = size(my_chain1);
[lenChain2, dimChain2] = size(my_chain2);

if( dimChain1 ~= dimChain2 )
    error('the dimensionality of the two chains is different')
end;

%--------------------------------------------------------------------------
% Estimate KL divergence
%--------------------------------------------------------------------------
[nn1,dd1] = knn(my_chain1, my_chain1, my_knn+1, inf);    
[nn2,dd2] = knn(my_chain2, my_chain1, my_knn, inf);    

my_dd1 = dd1(:,my_knn+1);
my_dd2 = dd2(:,my_knn);

log_ratio_dist = log(my_dd2 ./ my_dd1);
log_ratio_dist(isinf(log_ratio_dist)) = 0;
log_ratio_dist(isnan(log_ratio_dist)) = 0;

kl = dimChain1 / lenChain1 * sum(log_ratio_dist) + log(lenChain2/(lenChain1-1));