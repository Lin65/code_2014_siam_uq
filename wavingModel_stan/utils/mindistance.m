function dist = mindistance(oldindex, new, designList)


m = size(oldindex,2);

mindistance = inf;
for j = 1:m
    distance = sqrt((designList(new).x - designList(oldindex(j)).x)^2 + (designList(new).y - designList(oldindex(j)).y)^2);
    if distance < mindistance	
       mindistance = distance;
    end;
end;
    
dist = mindistance;
	

