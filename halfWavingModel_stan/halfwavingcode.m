model = StanModel('verbose',true);
model.compile('stanc');

code = {
    'data {'
        'int  N;'
        'real x[N];'
        'real y[N];'
        'vector[N] d;'
    '}'
    'parameters {'
        'real theta1;'                        
        'real theta2;'             
        'real theta3;'                   
        'real<lower=0.0000001> theta4;'   
        'real<lower=0.0000001> theta5;'
        'real<lower=0.0000001> theta6;'
    '}'
    'transformed parameters {'
        'vector[N] mu;'
        'cov_matrix[N] Sigma;'
        'for (i in 1:N)'
            'mu[i] <- theta1 + theta2*x[i] + theta3*y[i];'  
        'for (i in 1:N)' 
            'for (j in 1:N)'
                'if(i==j)'
                    'Sigma[i,j] <- fmax(0.0000001,pow(theta2,2));' 
                'else'
                    'Sigma[i,j] <- pow(theta4,2)*exp(-0.5*pow((x[i] - x[j]),2)/pow(theta5,2))*exp(-0.5*pow((y[i] - y[j]),2)/pow(theta6,2));' 
    '}'
    'model {'
        'theta1 ~ normal(0.0, 10);'
        'theta2 ~ normal(0.0, 10);'
        'theta3 ~ normal(0.0, 10);'
        'theta4 ~ normal(0.0, 0.5);'
        'theta5 ~ normal(0.0, 0.5);'
        'theta6 ~ normal(0.0, 0.5);'
        'd ~ multi_normal_cholesky(mu, Sigma);'
    '}'
};


model.set('model_code',code,'model_name','halfwavingModel');
model.compile();
